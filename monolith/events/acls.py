from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests

def get_pexels_images(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"
    headers = {"Authorization": PEXELS_API_KEY}
    try:
      response = requests.get(url, headers=headers)
      return response.json()
    except:
      return None

def get_weather(city, state):
    url = f"https://api.openweathermap.org/data/2.5/weather?q={city},{state},1&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    try:
        response = requests.get(url, headers=headers)
        response = response.json()
        weather = {
            "temperature": round(response["main"]["temp"]),
            "description": response["weather"][0]["description"],
        }
        return weather
    except:
        return None